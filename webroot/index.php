<? /**
 * Range-Ray API PROGRAMM 2016
 * @category   Range-Ray CMS
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @copyright  Copyright (c) 2014-2016, 3dchita.ru
 * @license    http://www.3dchita.ru/info/disclaimer
 * @link       http://www.3dchita.ru/projects/
 * ${FILENAME}
 * demo_shop
 * 2015-07-09
 */
ini_set('display_errors', 1);
require dirname(__DIR__) . '/src/bootstrap.php';

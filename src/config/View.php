<?php

/**
 * Created by PhpStorm.
 * User: rangeray
 * Date: 11.08.16
 * Time: 23:39
 */
class View

{

    public $layout = 'default'; // здесь можно указать общий вид по умолчанию.

    public $content_view;

    public $var;


    public function generate($layout, $content_view)
    {
        // set actions views
        $this->content_view = $content_view;


        if(file_exists(VIEWS . 'layouts/'. $layout . '.ctp')){

            include VIEWS . 'layouts/'. $layout . '.ctp';

        } else {

            throw new Exception('Отсутствует файл шаблон, создайте необходимый файл -->'. VIEWS . 'layouts/'. $layout . '.ctp');
        }

    }

    public function renderView(){

        if(file_exists(VIEWS. $this->content_view .'.ctp')){

            include VIEWS. $this->content_view .'.ctp';

        } else {

            throw new Exception('Отсутствует файл дествия контроллера, создайте необходимый файл -->'. VIEWS. $this->content_view .'.ctp');
        }



    }

    public function set($var, $data){
        $this->$var = $data ;
    }






}
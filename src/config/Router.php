<?php

/**
 * Created by PhpStorm.
 * User: rangeray
 * Date: 11.08.16
 * Time: 19:21
 */
class Router
{
    static function listen()
    {

        if(file_exists(CONTROLLERS.'AppController.php')){

            require_once CONTROLLERS.'AppController.php';

        } else {

            throw new Exception ('
            Не создан главный контроллер, создайте файл '. CONTROLLERS.'AppControllers.php
            ');
        }

        // default values
        $controller_name = 'products'; // контроллер по умолчанию
        $action_name = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);


        $params = isset($routes[3]) ? $routes[3] : null; // передаваемые параметры

        // get name controller
        !empty($routes[1]) ? $controller_name = $routes[1] : false;

        // get action
        !empty($routes[2]) ? $action_name = $routes[2] : false;

        // add_prefixes
        $model_name = ucfirst($controller_name) . 'Table';
        $origial_controller_name = $controller_name;
        $controller_name = ucfirst($controller_name) . 'Controller';

        //$action_name =  'action_' . $action_name;

        $model_file = $model_name . '.php';
        $model_patch = MODELS . $model_file;

        // include models
        if (file_exists($model_patch)) {

            require_once $model_patch;

        }

        $controller_file = $controller_name . '.php';
        $controller_path = CONTROLLERS . $controller_file;

        // include controllers
        //echo $controller_path;
        if (file_exists($controller_path)) {

            // file exist
            require_once $controller_path;

        } else {

            Router::ErrorPage404();
            throw new Exception('Контроллера (' . $controller_path .  ') не существует, создайте контроллер /app/src/controllers/'.$controller_file);

        }

        // create controller
        $controller = new $controller_name;
        $action = $action_name;


        if (method_exists($controller, $action)) {

            $controller->$action($params);
            $controller->initialize($origial_controller_name, $action); // render layout

        } else {
            Router::ErrorPage404();
            throw new Exception('Данный метод (' . $action . ') не поддерживается  контроллером');



        }


    }

    function ErrorPage404(){
        $host = 'http://'.$_SERVER['HTTP_HOST'].DS;

        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');
        header('Location:' . $host . '404');
    }
}
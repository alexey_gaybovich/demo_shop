<?php

/**
 * Created by PhpStorm.
 * User: rangeray
 * Date: 11.08.16
 * Time: 23:40
 */
class Model
{
    protected $data = [
        ['sku' => 1122],
        ['sku' => 1111],
        ['sku' => 2222],
        ['sku' => 3333],
        ['sku' => 4444],
        ['sku' => 1122],
        ['sku' => 3344],
        ['sku' => 1133],
    ];

    protected $default_data_cart = [
        [
            'sku' => 1111,
            'qty' => 3,
        ],
        [
            'sku' => 2222,
            'qty' => 3
        ],
        [
            'sku' => 3333,
            'qty' => 44
        ]
    ];

    protected $data_cart = array();

    public function get_data($data){
        return $this->$data;
    }

    public function generateXML(){

        // create dir if dir exist
        if(!file_exists(MODELS)){
            mkdir(MODELS, 0755);
        }

        if(!file_exists(MODELS.'cart.xml')){

            $this->writeCart($this->default_data_cart); // записываем данные по умолчанию


        }
    }

    public function readCart(){



        if(file_exists(MODELS.'cart.xml')){


            $cart = simplexml_load_file(MODELS.'cart.xml');


            foreach ($cart->item as $key => $item_val) {
               $this->data_cart[] = [
                   'sku' => (string)$item_val->sku,
                   'qty' => (string)$item_val->qty
                ];
            }

            return $this->data_cart;




        } else {
            $this->generateXML();
        }

    }

    public function writeCart($data = null){
        $dom = new domDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
        $root = $dom->createElement("cart"); // Создаём корневой элемент
        $dom->appendChild($root);

        foreach ($data as $item) {

            $cart_item = $dom->createElement("item");
            $cart_item->appendChild($dom->createElement('sku', $item['sku']));
            $cart_item->appendChild($dom->createElement('qty', $item['qty']));
            $root->appendChild($cart_item);
        }

        $dom->save(MODELS.'cart.xml');
    }

}
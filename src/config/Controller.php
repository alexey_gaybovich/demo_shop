<?php

/**
 * Created by PhpStorm.
 * User: rangeray
 * Date: 11.08.16
 * Time: 23:40
 */
class Controller
{
    public $model;

    public $view;

    function __construct(){
        $this->view = new View();
        $this->model = new Model();
    }

    function action_index(){
    }

    function redirect($controller, $action){
        header("Location: ".DS. $controller.DS.$action, true, 303);
    }


}
<? /**
 * Range-Ray API PROGRAMM 2016
 * @category   Range-Ray CMS
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @copyright  Copyright (c) 2014-2016, 3dchita.ru
 * @license    http://www.3dchita.ru/info/disclaimer
 * @link       http://www.3dchita.ru/projects/
 *
 * demo_shop
 */
?>

<table class="table">
    <caption>Корзина покупателя</caption>
    <thead>
    <tr>
        <th width="70%">артикул</th>
        <th>количество</th>
        <th>действие</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($this->cart as $item): ?>
        <tr>
            <td><?php echo $item['sku'] ?></td>
            <td colspan="2">
                <form class="form-inline" method="post" action="/products/delete">

                    <div class="form-group">
                        <input id="col" min="1" max="<?php echo $item['qty'] ?>" type="number" name="col" value="<?php echo $item['qty'] ?>" class="form-control" >
                        <label for="col">Шт.</label>
                    </div>



                    <input type="hidden" name="id" value="<?php echo $item['sku'] ?>">

                    <button  class="btn btn-danger pull-right" type="submit">Удалить</button>
                </form>
            </td>
        </tr>
    <? endforeach ?>
    </tbody>
</table>

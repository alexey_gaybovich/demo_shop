<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TEST WORK</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.26.4/css/uikit.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>




    </style>
</head>

<body>

<header>
    <div class="uk-container uk-container-center">
        <nav class="uk-navbar uk-margin-large-bottom">
            <div class="uk-navbar-content uk-navbar-center">
                <ul class="uk-navbar-nav uk-hidden-small">
                    <li><a href="#home">HOME</a></li>
                    <li><a href="#about me">ABOUT ME</a></li>
                    <li><a href="#portfolio">PORTFOLIO</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                </ul>
            </div>

            <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

            <div class="uk-navbar-brand uk-visible-small">MENU</div>
        </nav>
    </div>
    <div class="uk-visible-large down">
        <p>SCROLL DOWN TO SEE MORE</p>
        <i class="uk-icon-angle-double-down"></i>
    </div>
    <div class="line uk-visible-large"></div>
</header>

<div class="uk-container uk-container-center uk-hidden-small">
    <nav class="uk-navbar uk-margin-large-bottom">
        <div class="uk-navbar-content uk-navbar-center">
            <ul class="uk-navbar-nav uk-hidden-small tm-menu-gray">
                <li><a href="#home">HOME</a></li>
                <li><a href="#about me">ABOUT ME</a></li>
                <li><a href="#portfolio">PORTFOLIO</a></li>
                <li><a href="#contact">CONTACT</a></li>
            </ul>
        </div>

        <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

        <div class="uk-navbar-brand uk-visible-small">MENU</div>
    </nav>
</div>

<div class="line uk-visible-large"></div>

<div class="uk-container uk-container-center">
    <section>
        <div class="uk-grid">
            <div class="uk-width-large-1-2">
                <div class="tm-about"></div>
            </div>
            <div class="uk-width-large-1-2">
                <div class="tm-panel">
                    <h1>ABOUT ME</h1>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.
                    </p>

                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>

                    <p class="tm-name">
                        Jason Wood
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="uk-visible-large down">
            <p>keep scrolling, there is still more to come.</p>
            <i class="uk-icon-angle-double-down"></i>
        </div>
    </section>
</div>


<div id="offcanvas" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-offcanvas">
            <li><a href="#home">HOME</a></li>
            <li><a href="#about me">ABOUT ME</a></li>
            <li><a href="#portfolio">PORTFOLIO</a></li>
            <li><a href="#contact">CONTACT</a></li>
        </ul>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.26.4/js/uikit.min.js"></script>
<!-- Bootstrap core JavaScript
</body>
</html>
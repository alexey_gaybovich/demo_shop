<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TEST WORK</title>
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

    <link rel="stylesheet" href="/css/custom.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<header>

    <nav class="tm-navbar">
        <ul class="tm-navbar-nav tm-hidden-small">
            <li><a href="#home">HOME</a></li>
            <li><a href="#about me">ABOUT ME</a></li>
            <li><a href="#portfolio">PORTFOLIO</a></li>
            <li><a href="#contact">CONTACT</a></li>
        </ul>

        <ul class="tm-navbar-nav tm-visible-small">
            <li><a href="#contact">&infin; MENU</a></li>
        </ul>

    </nav>

    <div class="tm-visible-large down">
        <p>SCROLL DOWN TO SEE MORE</p>
        <i class="uk-icon-angle-double-down"></i>
    </div>
    <div class="line tm-visible-large"></div>
</header>
<nav class="tm-navbar tm-hidden-small">
    <div class="tm-navbar-center">
        <ul class="tm-navbar-nav gray">
            <li><a href="#home">HOME</a></li>
            <li><a href="#about me">ABOUT ME</a></li>
            <li><a href="#portfolio">PORTFOLIO</a></li>
            <li><a href="#contact">CONTACT</a></li>
        </ul>
    </div>
</nav>


<div class="line tm-visible-large"></div>

<div class="tm-wrapper">
    <section>
        <div class="tm-grid">
            <div class="tm-width-large-1-2">
                <div class="tm-about"></div>
            </div>
            <div class="tm-width-large-1-2">
                <div class="tm-panel">
                    <h1>ABOUT ME</h1>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.
                    </p>

                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>

                    <p class="tm-name">
                        Jason Wood
                    </p>
                </div>
            </div>
        </div>


        <div class="tm-position-relative tm-block">
            <div class="tm-hidden-small down">
                <p>keep scrolling, there is still more to come.</p>
                <i class="uk-icon-angle-double-down"></i>
            </div>
        </div>

    </section>
</div>




</body>
</html>
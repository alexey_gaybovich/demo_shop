<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DEMO SHOP</title>

    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->

</head>

<body>

<header>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="center-bloc collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#home">HOME</a></li>
                    <li><a href="#about me">ABOUT ME</a></li>
                    <li><a href="#portfolio">PORTFOLIO</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <nav class="uk-navbar">

        <div class="uk-navbar-content uk-navbar-center">
            <ul class="uk-navbar-nav">
                <li><a href="">HOME</a></li>
                <li><a href="">ABOUT ME</a></li>
                <li><a href="">PORTFOLIO</a></li>
                <li><a href="">CONTACT</a></li>
            </ul>
        </div>

    </nav>

    <div class="down">
        <p>SCROLL DOWN TO SEE MORE</p>
        <i class="uk-icon-angle-double-down"></i>
    </div>
</header>


<div class="container">
    <?php echo $this->renderView() ?>
</div><!-- /.container -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.26.4/js/uikit.min.js"></script>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>


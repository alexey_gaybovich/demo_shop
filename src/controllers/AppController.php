<? /**
 * Range-Ray API PROGRAMM 2016
 * @category   Range-Ray CMS
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @copyright  Copyright (c) 2014-2016, 3dchita.ru
 * @license    http://www.3dchita.ru/info/disclaimer
 * @link       http://www.3dchita.ru/projects/
 * ${FILENAME}
 * demo_shop
 * 2015-07-09
 */
class AppAppController extends Controller{

    public $origial_controller_name;

    public $action;

    public $layout = 'default';

    public function initialize($origial_controller_name, $action){

        if($origial_controller_name === 'page'){
            $this->layout = 'custom';
        }
        $this->view->generate($this->layout, $origial_controller_name.DS.$action); // generate default
    }

}


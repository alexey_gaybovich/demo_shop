<?php

/**
 * Created by PhpStorm.
 * User: rangeray
 * Date: 11.08.16
 * Time: 18:57
 */
class ProductsController extends AppAppController
{
    public function index(){

        $data = $this->model->get_data('data');
        $this->view->set('data', $data);

    }

    public function addCart($params){

        if(!$params){
            // нет параметров редеректим на главную
            $this->redirect('products', 'index');
        }

        $cart = $this->model->readCart();
        $key = array_search($params, array_column($cart, 'sku')); // ищем значении



        if($key === false){ // проверка естли данные

            /*echo '<pre>';
           echo 'ключ пустой';
           print_r($key);
           echo '</pre>';

           echo '<pre>';
               print_r($cart);
           echo '</pre>';*/

            $item = [
                'sku' => $params,
                'qty' => 1,
            ];

            $cart[] = $item; // добавляем елемент

            $this->model->writeCart($cart); // пишем в файл


            // перенаправляем в корзину
            //$this->redirect('products', 'cart');


        } else {

            /*echo '<pre>';
            echo 'ключ не пустой!';
            print_r($key);
            echo 'ключ найден! ' . $cart[$key]['sku'];
            echo '</pre>';

            /*echo '<pre>';
            print_r($cart);
            echo '</pre>';*/

            $cart[$key]['qty'] = $cart[$key]['qty'] + 1; // прибавлем количество

            $this->model->writeCart($cart); // пишем в файл

            // перенаправляем в корзину
            //$this->redirect('products', 'cart');

        }



        $this->redirect('products', 'cart');
    }

    public function cart(){

        $this->view->set('cart',  $this->model->readCart()); // читаем корзину и отправляем в view


    }

    public function delete(){
        if(!$_POST){
            // метод неподдерживается редеректим на главную
            $this->redirect('products', 'index');
        }
        


        if(!isset($_POST['col']) && !isset($_POST['id'])){ // если параметры несуществуют
            // пустые параметры запроса редеректим на главную
            $this->redirect('products', 'index');
        }

        if($_POST['col'] <= 0 && is_numeric($_POST['col'])){ // проверка количества удалемого товара и что это число
            // количаство удаляемых элементов не может быть меньше либо равно нулю
            $this->redirect('products', 'index');
        }

        if($_POST['id'] <= 0 && is_numeric($_POST['id'])){
            // артикул не может быть меньше либо равно нулю либо числом
            $this->redirect('products', 'index');
        }

        $cart = $this->model->readCart();
        $key = array_search($_POST['id'], array_column($cart, 'sku')); // ищем значении

        if($key === false){
            // удаляемый элемент не найден
            $this->redirect('products', 'index');
        }

        if($_POST['col'] > $cart[$key]['qty']){ // проверка количесва удаляемого товара
            // проверка что количество удаляемых больше количеству товаров из корзины
            $this->redirect('products', 'index');
        }

        // все проверки пройдены производим удаление

        $cart[$key]['qty'] = $cart[$key]['qty'] - $_POST['col'];

        if($cart[$key]['qty'] === 0){
            unset($cart[$key]); // удаляем элемент
        }

        $this->model->writeCart($cart); // пишем в файл
        // опрерация выполнена
        $this->redirect('products', 'cart');

        /*echo '<pre>';
        print_r($_POST);
        echo '</pre>';*/


    }

}
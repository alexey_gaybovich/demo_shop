<?php /**
 * Range-Ray API PROGRAMM 2016
 * @category   Range-Ray CMS
 * @author     gaibovich.alexey <rangeray@mail.ru>
 * @copyright  Copyright (c) 2014-2016, 3dchita.ru
 * @license    http://www.3dchita.ru/info/disclaimer
 * @link       http://www.3dchita.ru/projects/
 * ${FILENAME}
 * demo_shop
 * 2015-07-09
 */
ini_set('display_errors', 1);
// create constants
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}


// root /app
define('ROOT', dirname(__DIR__));

// source dir
define('APP_DIR', 'src');

// full path to src /app/src/
define('APP', ROOT . DS . APP_DIR . DS);

// full path to webroot /app/webroot/
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

// full path to config directory /app/src/config/
define('CONFIG', APP . 'config' . DS);

// full path to controllers directory /app/src/controllers/
define('CONTROLLERS', APP . 'controllers' . DS);

// full path to models directory /app/src/models/
define('MODELS', APP . 'models' . DS);

// full path to views directory /app/src/views/
define('VIEWS', APP . 'views' . DS);

// define request q
define('Q', isset($_REQUEST['q']) ? $_REQUEST['q'] : DS);

//autoload

require_once (CONFIG . 'Router.php');
require_once (CONFIG . 'Controller.php');
require_once (CONFIG . 'View.php');
require_once (CONFIG . 'Model.php');

$model = new Model();
$model->generateXML(); // генерируем xml если он отсутствует



Router::listen(); // инициализация роутера